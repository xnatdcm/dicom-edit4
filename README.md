NRG DICOM Edit 4
================================

The NRG DICOM Edit 4 library provides support for extracting and modifying the
the DICOM header metadata and values in DICOM file sets.

Building
--------

To build NRG DICOM Edit 4, invoke Maven with the desired lifecycle phase.  For 
example, the following command cleans previous builds, builds a new jar file, 
creates archives containing the source code and JavaDocs for the library, runs
the library's unit tests, and installs the jar into the local repository:

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mvn clean install
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
